%Implement for dual optimization problem
function [w, b, dual_alpha] = linear_hard_svm_dual(X, y)
%linear hard margin svm
%let we have m datapoints in R^n. 
[m, n] = size(X);
    cvx_begin
        variables dual_alpha(m);
        minimize( 0.5.*quad_form(y.*dual_alpha, X*X') - sum(dual_alpha) );
        subject to
            y'*(dual_alpha) == 0;
            dual_alpha >= 0;
    cvx_end
    
    ind = find(dual_alpha > 1e-3);
    w = sum(y(ind).*dual_alpha(ind).*X(ind, :)).';
    b = mean(1./y(ind)-X(ind, :)*w);
end    
        
    
        
