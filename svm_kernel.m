%model is a struct which stores information about type of kernel and
%parameters specific to that type of kernel
function K = svm_kernel(X1,X2, kernel)
    K = zeros(size(X1, 1), size(X2, 1));
    
    for i = 1:size(X1, 1)
        for j = 1:size(X2, 1)
            if strcmp(kernel.type, 'linear')
                K(i, j) = X1(i, :) * X2 (j, :).';
            end
            
            if strcmp(kernel.type, 'polynomial')
                K(i, j) = (1 + X1(i, :) * X2 (j, :).')^kernel.order;
            end
            
            if strcmp(kernel.type, 'rbf')
                diff = X1(i, :) - X2 (j, :);
                K(i, j) = exp((-1.0 * (diff * diff.'))/(2*kernel.sigma^2));
            end
        end
    end
end