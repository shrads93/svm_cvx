function predicted = linear_svm_predict(X, model)
%linear_svm_predict predicts output of linear svm on data points X. 
%   model is assumed to be a struct with fields 'w' and 'b' of svm.

    w = model.w;
    b = model.b;
    
    %sign(wx+b)
    predicted = int8((X*w + b) > 0);
    predicted(predicted==0) = -1;
    
end