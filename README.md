# README #

### What is this repository for? ###

* This repository provide implementation for different versions of Support Vector Machines (SVM) using CVX toolbox in Matlab. 
  It supports soft and hard SVM for both linear and non-linear case.
* Version 1.0


### How do I get set up? ###

* Please check file SVM.m which provide usage examples.

### Contribution guidelines ###

* Suggestions and Imporvements are always welcome. Please raise a pull request if you have any.

### Who do I talk to? ###

* Please contact at agrawalshradha78@gmail.com if you have any queries or suggestions.