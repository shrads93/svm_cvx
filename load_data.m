function [X, y] = load_data(name)
    switch name
        case 'fisheriris'     
            %fisher iris dataset
            load fisheriris.mat
            X = meas;
            y = grp2idx(species);

            %take only two species 1:100 and first 2 attributes
            X = X(1:100, 1:2);
            y = y(1:100);

            %convert labels to {-1, 1}
            %y(y==1) = -1;
            y(y==2) = -1;
            
        case 'wine'
            %-------------Wine dataset--------------
            load wine_dataset;
            X = wineInputs([1,11], :).';
            %label wine1 to 1 and others to -1
            %this will result in unequal number of examples in both classes
            [M, I] = max(wineTargets);
            y = I';
            y(y==2 | y == 3) = -1;
        
        case 'data1'
            %-------250 B, data1-----------
            load data1.txt
            X = data1(:, 1:2);
            y = data1(:, 3);
            
        case 'data2'
            %-------250 B, data2-----------
            load data2.txt
            X = data2(:, 1:2);
            y = data2(:, 3);
    end
end


