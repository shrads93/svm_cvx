function [w, b, primal_alpha, e] = linear_soft_svm(X, y)
%linear hard margin svm
%let we have m datapoints in R^n. 
[m, n] = size(X);
C = 0.1;
    cvx_begin
        variables w(n) e(m) b;
        dual variable primal_alpha;
        minimize( 0.5 * w'*w  + C * sum(e) );
        subject to
            primal_alpha: y.*(X*w + b) - ones(m, 1)+ e >= 0;
            e >= 0; %slack
    cvx_end
end