function SVM(X, y)
    
    %load data for linear case
    [X, y] = load_data('fisheriris');
    
    %hard svm for linear case
    [w_hp, b_hp, alpha_hp] = linear_hard_svm(X, y);
    [w_hd, b_hd, alpha_hd] = linear_hard_svm_dual(X, y);
    
    %soft svm for linear case
    [w_sp, b_sp, alpha_sp] = linear_soft_svm(X, y);
    [w_sd, b_sd, alpha_sd] = linear_soft_svm_dual(X, y);
    
    %load data for non-linear case
    [X, y] = load_data('wine');
    
    %prepare rbf kernel for non-linear case
    kernel = struct('type', 'rbf', 'order', 2, 'sigma', 0.3);
    K = svm_kernel(X, X, kernel);
    
    %hard svm for non-linear case
    [b_nh, alpha_nh] = nonlinear_hard_svm(K, y);
    ind = find(alpha_nh > 1e-3);
    support = struct('alpha', alpha_nh(ind), 'X', X(ind, :), 'y', y(ind),'b', b_nh);
    visualizeBoundary(X, y, support, kernel);
    
    %soft svm for non-linear case
    [b_ns, alpha_ns] = nonlinear_soft_svm(K, y);
    ind = find(alpha_ns > 1e-3);
    support = struct('alpha', alpha_ns(ind), 'X', X(ind, :), 'y', y(ind),'b', b_ns);
    visualizeBoundary(X, y, support, kernel);
    
end
