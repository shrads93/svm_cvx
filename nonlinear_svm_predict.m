function predicted = nonlinear_svm_predict(X, support, kernel)
    
    support_vectors = support.alpha .* support.y .* support.X;
    K = svm_kernel(support_vectors, X, kernel);
    
    %sign(col sum of K + b)
    predicted = int8((sum(K).' + support.b) > 0);
    predicted(predicted==0) = -1;
    
end