function [b, dual_alpha] = nonlinear_soft_svm(K, y)
%linear hard margin svm
%let we have m datapoints in R^n. 
[m, n] = size(K);
C = 1.0;
    cvx_begin
        variables dual_alpha(m);
        minimize( 0.5.*quad_form(y.*dual_alpha, K) - sum(dual_alpha) );
        subject to
            y'*(dual_alpha) == 0;
            0 <= dual_alpha <= C;
    cvx_end
    
    ind = find(dual_alpha > 1e-3 & dual_alpha < C - 1e-3);
    b = mean(1./y(ind)-sum(y(ind).*dual_alpha(ind).*K(ind, ind), 2));
    
end