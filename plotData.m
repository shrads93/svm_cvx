function plotData(X, y, predicted, alpha)
%PLOTDATA Plots the data points X and y into a new figure 
%   PLOTDATA(x,y) plots the data points with + for the positive examples
%   and o for the negative examples. X is assumed to be a Mx2 matrix.
%
% Note: This was slightly modified such that it expects y = 1 or y = -1

% Find Indices of Positive and Negative Examples
pos = find(y == 1); neg = find(y == -1);
support = find(alpha > 1e-3);
error = find(y ~= predicted);

% Plot Examples
plot(X(pos, 1), X(pos, 2), 'k+','LineWidth', 1, 'MarkerSize', 7)
hold on;
plot(X(neg, 1), X(neg, 2), 'ko', 'MarkerFaceColor', 'y', 'MarkerSize', 7)
hold on;
plot(X(support, 1), X(support, 2), 'ko', 'MarkerEdgecolor', 'r', 'MarkerSize', 8)
hold on;
plot(X(error, 1), X(error, 2), 'kx', 'LineWidth', 2, 'MarkerSize', 8)
hold off;

end