function visualizeBoundaryLinear(X, y, model)
%VISUALIZEBOUNDARYLINEAR plots a linear decision boundary learned by the
%SVM
%   VISUALIZEBOUNDARYLINEAR(X, y, model) plots a linear decision boundary 
%   learned by the SVM and overlays the data on it

w = model.w;
b = model.b;
alpha = model.alpha;
predicted = linear_svm_predict(X, model);

xp = linspace(min(X(:,1))-1, max(X(:,1))+1, 100);
yp = - (w(1)*xp + b)/w(2);
yp_pos = (1 - w(1)*xp - b)/w(2);
yp_neg = -(1 + w(1)*xp + b)/w(2);

plotData(X, y, predicted, alpha);
hold on;
plot(xp, yp, xp, yp_pos,'--',xp,yp_neg,':'); 
hold off

end