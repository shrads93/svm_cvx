function [w, b, primal_alpha] = linear_hard_svm(X, y)
%linear hard margin svm
%let we have m datapoints in R^n. 
[m, n] = size(X);
    cvx_begin
        variables w(n) b;
        dual variable primal_alpha;
        minimize( 0.5 * w'*w );
        subject to
            primal_alpha: y.*(X*w + b) - ones(m, 1) >= 0;
    cvx_end
end