function [w, b, dual_alpha] = linear_soft_svm_dual(X, y)
%linear hard margin svm
%let we have m datapoints in R^n. 
[m, n] = size(X);
C = 1.0;
    cvx_begin
        variables dual_alpha(m);
        minimize( 0.5.*quad_form(y.*dual_alpha, X*X') - sum(dual_alpha) );
        subject to
            y'*(dual_alpha) == 0;
            0 <= dual_alpha <= C;
    cvx_end
    
    ind = find(dual_alpha > 1e-3);
    w = sum(y(ind).*dual_alpha(ind).*X(ind, :)).';
    
    %consider only those support vectors for which alpha are less than C
    ind = intersect(ind, find(dual_alpha < C - 1e-3))
    b = mean(1./y(ind)-X(ind, :)*w);
    
end